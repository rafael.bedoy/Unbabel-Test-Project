import unittest
import HTMLTestRunner
import os
import time
from TestCases import UnbabelTestCases

# get the directory path to output report file
dir = os.getcwd()

# get all tests from UnbabelTestCases

test_cases = unittest.TestLoader().loadTestsFromTestCase(UnbabelTestCases)

#Creating s test suite
smoke_test = unittest.TestSuite([test_cases])
# open the report file
dateTimeStamp = time.strftime("%Y%m%d_%H_%M_%S")

outfile = open(dir + "\Results\SmokeTestReport" + "_" + dateTimeStamp + ".html", "w")
# run the suite
#unittest.TextTestRunner(verbosity=2).run(smoke_test)

runner = HTMLTestRunner.HTMLTestRunner(stream = outfile, verbosity = 1, title="Regression Suite")
runner.run(smoke_test)