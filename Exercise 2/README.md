# Annotation Tool Test Plan is upladed as .docx document, named Test_Plan_Ubabel.docx
## Test plan was created in a very basic way, if you guys believe that I need to be more specific or add anything else, please let me know.


# Test Cases

## TC001_Login Happy Path

|Step #:|Step Description:|Data (If needed):|Expected Results:|
|:-----:|:----------------:|:----------------:|:----------------:|
|1|Go to Unbabel Annotate Tool Web Site|[Unbabel Annotate Tool](https://staging.annotation.tools.unbabel.com/)|Unbabel Annotate Tool main screen will be shown.|
|2|On Main screen Click on `Sing In` button||Unbabel Annotate Login page will get loaded|
|3|Type a **valid** User on `Username` text field|emanuel+rafa@unbabel.com|Username field should be enabled for typing any key|
|4|Type the **valid** password for entered user on `Password` text field then Click on `Sing In` button|Abot&Babu5|Login will be successful and Batches page will get loaded.|

## TC002_Login Invalid Data

|Step #:|Step Description:|Data (If needed):|Expected Results:|
|:-----:|:----------------:|:----------------:|:----------------:|
|1|Go to Unbabel Annotate Tool Web Site|[Unbabel Annotate Tool](https://staging.annotation.tools.unbabel.com/)|Unbabel Annotate Tool main screen will be shown.|
|2|On Main screen Click on `Sing In` button||Unbabel Annotate Login page will get loaded|
|3|Type an **Invalid** User on `Username` text field|rafa@unbabel.com|Username field should be enabled for typing any key|
|4|Type an **Invalid** password for entered user on `Password` text field then Click on `Sing In` button|Testing|An error message will be displayed:**Warning! Invalid Credentials. Please try again.**|

## TC003_Login Invalid Data2

|Step #:|Step Description:|Data (If needed):|Expected Results:|
|:-----:|:----------------:|:----------------:|:----------------:|
|1|Go to Unbabel Annotate Tool Web Site|[Unbabel Annotate Tool](https://staging.annotation.tools.unbabel.com/)|Unbabel Annotate Tool main screen will be shown.|
|2|On Main screen Click on `Sing In` button||Unbabel Annotate Login page will get loaded|
|3|Type a **valid** User on `Username` text field|emanuel+rafa@unbabel.com|Username field should be enabled for typing any key|
|4|Type an **Invalid** password for entered user on `Password` text field then Click on `Sing In` button|Testing|An error message will be displayed:**Warning! Invalid Credentials. Please try again.**|

## TC004_Choose one task

|Step #:|Step Description:|Data (If needed):|Expected Results:|
|:-----:|:----------------:|:----------------:|:----------------:|
|1|Run TC: TC001_Login Happy Path||See expected results on TC|
|2|Verify that data was loaded properly|[Screenshot](Exercise 2/TC004_Step02.jpg)|Batch name, Date of creation and Progress bar should be displayed|
|3|Click on `emanuel+rafa` button||Logout option will be displayed|
|4|Click on Any batch from the list||Batch information will get loaded|

## TC005_ Verify annotation screen

|Step #:|Step Description:|Data (If needed):|Expected Results:|
|:-----:|:----------------:|:----------------:|:----------------:|
|1|Run TC: TC001_Login Happy Path||See expected results on TC|
|2|Verify that upper bar was loaded properly||Back button, Register type and timer should be displayed|
|3|Verify that Left pane was loaded properly||Information, Annotations and Proposed Glossary Terms options should be available for expand|
|4|Verify that Information section should be expanded and is showing the right data||Register type and instructions will be displayed.|
|5|On the left pane of the screen Click on `+ Annotations` option||Information section should get collapsed and Annotations section should get expanded.|
|6|Verify if Annotations are listed and that they are sorted by position by default||Annotations should be ordered by Position by default.|
|7|On **Sort by** section Click on `Type` option||Annotations will get sorted by Type (Alphabetically)|
|8|On **Sort by** section Click on `Severity` option||Annotations will get sorted by Severity|
|9|On the left pane of the screen Click on `+ Proposed Glossary Terms` option||Annotations section should get collapsed and Glossary Terms section should get expanded|
|10|Verify that `Annotate` section on the right pane is expanded by default||`Annotate` section must be expanded by default|
|11|From the center pane, select some text with the mouse||On Annotate section, the text selected will be displayed|
|12|On the Right pane of the screen Click on `+ Proposed Glossary Terms` option||Annotate section should get collapsed and Glossary Terms section should get expanded|
|13|On the Right pane of the screen Click on `+ Finish or Report` option||Glossary Terms section should get collapsed and Finish or Report should get expanded|


## TC006_ Perform annotation

|Step #:|Step Description:|Data (If needed):|Expected Results:|
|:-----:|:----------------:|:----------------:|:----------------:|
|1|Run TC: TC005_ Verify annotation screen||See expected results on TC|
|2|From the center pane, select some text with the mouse||On Annotate section, the text selected will be displayed|
|3|Select any option from `Error Type` section|Accuracy-> Omission-> Omitted Preposition| option should get selected|
|4|Select any `Severity` option|Major|`ADD` button should get enabled|
|5|Click on `ADD` button||Annotation should be added|

## TC007_ Take a Break
**Need Info for this one**

## TC008_ Resume Annotation
**Need Info for this one too**

## TC009_ Finish annotation

|Step #:|Step Description:|Data (If needed):|Expected Results:|
|:-----:|:----------------:|:----------------:|:----------------:|
|1|Run TC: TC006_ Perform annotation||See expected results on TC|
|2|On right pane click on `+ Finish or Report` option||Glossary Terms section should get collapsed and Finish or Report should get expanded|
|3|Add any comment on `Task comment` text field and Click `Finish` button||**Do you want to finish this job?** message will ask for answer (No-YES)|
|4|Click on `YES` button||Annotation will get finished|