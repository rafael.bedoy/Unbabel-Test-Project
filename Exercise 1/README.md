# Rafa's QA Engineer Challenge
## 1. Requirement Analysis
### 1.1 Review and Refactor
### Provide 5 specific examples of things you would change, why and how:
1. Add a Glossary.
    - Why?: All the documents should have an easy access to all the sections
    instead of scrolling down until you find the section you are looking for.
    - How?: Just create a glossary that will show all the sections of the document
    and the user nust be able to click on each section in order to jump in to it.

2. Software Version Should be included.
    - Why?: In order for the user to know if the documentation is checking will be updated is needed to show 
    on which version of software the information was generated for.
    - How?: On the first page always have to show the software version and the date when it was released, even if it's only a 
    release candidate or beta.

3. Update Annotation Page help.
    - Why?: It is not updated.
    - How?: Add the information updated on the Guide. 

4. Update the Annotation Tool link at the end of the guide.
    - Why?: The link is broken http://error-check.nlp.unbabel.com/
    - How?: Just update the link to the current site.