import unittest
from random import randrange
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

class UnbabelTestCases(unittest.TestCase):
    def setUp(self):
        #adding variables
        self.user="emanuel+rafa@unbabel.com"
        self.pwd="Abot&Babu5"
        # Create Chrome session
        self.driver = webdriver.Chrome("C:/Users/rbedoyt/chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

        # Navigate to Unbabel Annotation Tool
        self.driver.get("https://staging.annotation.tools.unbabel.com/")


    def test_Create_Annotation(self):
        self.test_login()
        #self.User_Button = self.driver.find_element_by_css_selector("button.btn-round.button-navbar")
        self.User_Button = self.driver.find_element_by_xpath("//div[@class='btn-group']//button[@type='button']")
        self.User_Button.click()
        print("Checking Logout Button.")
        self.Logout_Button = self.driver.find_element_by_xpath("//a[@href='/logout/']")
        self.assertEqual(self.Logout_Button.text.strip(),"Logout")
        self.Progress_Bar = self.driver.find_element_by_xpath("//div[@class='progress-bar']")
        if self.Progress_Bar.is_displayed():
            self.driver.save_screenshot("Results\Screenshot_Select_a_batch.png")

        self.Batch_Link = self.driver.find_element_by_xpath("//a[@href='/interface/5b0c1e84c86b930046baaaeb/']")
        self.Batch_Link.click()
        print("Selecting Batch.")
        self.driver.save_screenshot("Results\Screenshot_test_verify_Batch.png")
        self.Spanish_Text = self.driver.find_element_by_xpath("//html//div[2]/div[2]/div[2]/div[1]/p[1]")
        if self.Spanish_Text.is_displayed():
            print("Text Found: " + self.Spanish_Text.text)
        #Selecting text to be added to the annotation
        self.actions = ActionChains(self.driver)

        self.actions.move_to_element_with_offset(self.Spanish_Text, 50, 50)
        self.actions.click_and_hold(on_element=None)
        self.Increase = 50 + randrange(10,50,10)
        self.actions.move_by_offset(self.Increase, self.Increase)
        self.actions.release()
        self.actions.perform()
        self.driver.implicitly_wait(10)
        print("Text selection")
        self.driver.save_screenshot("Results\Screenshot_Text_Selected_for_Annotation.png")
        # Selecting Error Type
        self.Error_Type_Section = self.driver.find_element_by_xpath("//span[@class='c-MarkError__errorType__name'][contains(text(),'Accuracy')]").click()
        self.Error_Type = self.driver.find_element_by_xpath("//span[@class='c-MarkError__errorType__name'][contains(text(),'Addition')]")
        self.Error_Type.click()
        print("Selecting Error Type: Addition.")

        ##Select Severity
        self.Severity = self.driver.find_element_by_xpath("//input[@id='minorSeverity']")
        self.Severity.click()
        print("Selecting Severity: Minor.")

        #Adding Annotation
        self.Add_Button = self.driver.find_element_by_xpath("//button[@class='c-Button c-Button--cta'][contains(text(),'Add')]")
        self.Add_Button.click()
        print("Annotation Added.")
        self.driver.save_screenshot("Results\Screenshot_Adding_Annotation_Final.png")




    def test_login(self):
        self.LogIn_Button = self.driver.find_element_by_css_selector("button.btn-round.btn-white-home")
        self.LogIn_Button.click()
        print("LogIn Button Clicked")

        # Enter User and Pwd
        self.UserField = self.driver.find_element_by_id("username")
        self.PwdField = self.driver.find_element_by_id("password")
        self.UserField.send_keys(self.user)
        print("UserName Entered")
        self.PwdField.send_keys(self.pwd)
        print("Password Entered")
        self.PwdField.send_keys(Keys.ENTER)

