# Load Test Repost and Files.
## I finally was able to run the Load test, I've attache some files as evidense of it.
## Ubabel_Load_test.jmx file is the project I create for the test.
### According to the info on the Exercise description this is how it should work:

### Load Testing
### It's expected that this service must support 75 users at any given moment and must be able to handle 350 requests per second. The response time must be under 200 ms.
### I ran a Load test using 75 users sending 100 request per second and you will see the results on the graph Image attached Aggregate Graph.png. 

# Automation:
## I've been working with Selenium, creating Python code so I uploaded the project [here](https://gitlab.com/rafael.bedoy/Unbabel-Test-Project/tree/master/Exercise%203/Automation%20Scripts)
### You just need to open TestCasesExecution.py and execute it.
### I've added the Test Cases on TestCases.py *Just need to add the one related with "Finish Annotation" need more info*
### The results will be generated on the "Results" folder, also screenshots will be saved there.